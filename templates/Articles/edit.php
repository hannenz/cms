<h1>Edit article</h1>
<?php
echo $this->Form->create($article);
echo $this->Form->control('user_id', ['type' => 'hidden']);
echo $this->Form->control('title');
echo $this->Form->control('body', ['rows' => '8']);
// echo $this->Form->control('tags._ids', ['options' => $tags]);
echo $this->Form->control('tag_string', ['type' => 'text']);
echo $this->Form->button(__('Save article'));
echo $this->Form->end();
?>
