<h1>
Articles tagged with <?php echo $this->Text->toList(h($tags), 'or'); ?>
</h1>

<section>
    <?php foreach ($articles as $article): ?>
    <article>
        <h4><?php echo $this->Html->link(
            $article->title,
            [
                'controller' => 'Articles',
                'action' => 'view',
                $article->slug
            ]);
            ?>
        </h4>
        <span><?php echo h($article->created) ?></span>
    </article>
    <?php endforeach ?>
</section>
