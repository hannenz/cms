<?php
namespace App\Controller;

use App\Controller\AppController;

class ArticlesController extends AppController {

    /**
     * undocumented function
     *
     * @return void
     */
    public function initialize(): void {
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');
    }


    public function index() {
        $this->loadComponent('Paginator');
        $articles = $this->Paginator->paginate($this->Articles->find());
        $this->set(compact('articles'));
    }


    /**
     * undocumented function
     *
     * @return void
     */
    public function view($slug = null) {
        $article = $this->Articles
                        ->findBySlug($slug)
                        ->contain('Tags')
                        ->firstOrFail();
        $this->set(compact('article'));
    }

    /**
     * undocumented function
     *
     * @return void
     */
    public function add() {
        $article = $this->Articles->newEmptyEntity();
        if ($this->request->is('post')) {
            $article = $this->Articles->patchEntity($article, $this->request->getData());
            $article->user_id = 1;

            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Article has been saved'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Saving article failed'));
        }
        $tags = $this->Articles->Tags->find('list')->all();
        $this->set('tags', $tags);
        $this->set('article', $article);
    }


    /**
     * undocumented function
     *
     * @return void
     */
    public function edit($slug) {
        $article = $this->Articles->findBySlug($slug)->contain('Tags')->firstOrFail();

        if ($this->request->is(['post', 'put'])) {
            $this->Articles->patchEntity($article, $this->request->getData());
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('Article has been saved'));
                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Saving article failed'));
        }
        $tags = $this->Articles->Tags->find('list')->all();
        $this->set('tags', $tags);
        $this->set('article', $article);
    }


    /**
     * undocumented function
     *
     * @return void
     */
    public function delete($slug) {
        $this->request->allowMethod(['post', 'delete']);
        $article = $this->Articles->findBySlug($slug)->firstOrFail();
        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('The {0} article has been deleted.', $article->title));
            return $this->redirect(['action' => 'index']);
        }
    }



    /**
     * undocumented function
     *
     * @return void
     */
    public function tags() {
        $tags = $this->request->getParam('pass');
        $articles = $this->Articles->find('tagged', [
            'tags' => $tags
        ])
        ->all();
        $this->set([
            'articles' => $articles,
            'tags' => $tags
        ]);
    }

}
